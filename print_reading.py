#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re

from database import Session, Reading
from utils import PrependStream

def main():
    reading_id = int(sys.argv[1])
    format_txt = len(sys.argv) >= 3 and sys.argv[2] == 'format_txt'
    session = Session()
    reading = session.query(Reading).filter(Reading.id == reading_id).one()
    text = reading.text
    fout = PrependStream(sys.stdout, '    ') if not format_txt else sys.stdout
    fout.write(('"' if not format_txt else '') + text.strip() + ('"' if not format_txt else ''))
    sys.stdout.write('\n')
    if format_txt:
        sys.stdout.write('\n--\n\n(%s)\n' % (reading.quote.replace('Sal', 'Salmo')))
    session.rollback()

if __name__ == '__main__':
    main()
