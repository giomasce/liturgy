#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime

from constants import *
from movable_dates import get_pentecost, get_saint_family
from utils import int_to_roman

WEEKDAYS_ITALIAN = {
    WD_MONDAY: 'lunedì',
    WD_TUESDAY: 'martedì',
    WD_WEDNESDAY: 'mercoledì',
    WD_THURSDAY: 'giovedì',
    WD_FRIDAY: 'venerdì',
    WD_SATURDAY: 'sabato',
    WD_SUNDAY: 'domenica',
}

SEASONS_ITALIAN = {
    SEASON_ADVENT: 'tempo di avvento',
    SEASON_CHRISTMAS: 'tempo di Natale',
    SEASON_ORDINARY_I: 'tempo ordinario',
    SEASON_LENT: 'tempo di quaresima',
    SEASON_EASTER: 'tempo di Pasqua',
    SEASON_ORDINARY_II: 'tempo ordinario',
}

BASE_TITLE_ITALIAN = '%s della %s settimana del %s'

GENERAL_CALENDAR_LIST = [
    (1, 1, "Maria SS. Madre di Dio", TYPE_SOLEMNITY),
    (1, 2, "Ss. Basilio Magno e Gregorio Nazianzeno, vescovi e dottori della Chiesa", TYPE_MEMORY),
    (1, 3, "SS. Nome di Gesù", TYPE_OPTIONAL_MEMORY),
    #(1, 6, u"Epifania del Signore", TYPE_SOLEMNITY),
    (1, 7, "S. Raimondo da Peñafort", TYPE_OPTIONAL_MEMORY),
    (1, 13, "S. Ilario, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (1, 17, "S. Antonio, abate", TYPE_MEMORY),
    (1, 20, "S. Fabiano, papa e martire", TYPE_OPTIONAL_MEMORY),
    (1, 20, "S. Sebastiano, martire", TYPE_OPTIONAL_MEMORY),
    (1, 21, "S. Agnese, vergine e martire", TYPE_MEMORY),
    (1, 22, "S. Vincenzo, diacono e martire", TYPE_OPTIONAL_MEMORY),
    (1, 24, "S. Francesco di Sales, vescovo e dottore della Chiesa", TYPE_MEMORY),
    (1, 25, "Conversione di S. Paolo, apostolo", TYPE_FEAST),
    (1, 26, "Ss. Timòteo e Tito, vescovi", TYPE_MEMORY),
    (1, 27, "S. Angela Merici, vergine", TYPE_OPTIONAL_MEMORY),
    (1, 28, "S. Tommaso d'Aquino, sacerdote e dottore della Chiesa", TYPE_MEMORY),
    (1, 31, "S. Giovanni Bosco, sacerdote", TYPE_MEMORY),

    (2, 2, "Presentazione del Signore", TYPE_LORD_FEAST),
    (2, 3, "S. Biagio, vescovo e martire", TYPE_OPTIONAL_MEMORY),
    (2, 3, "S. Ansgario (Oscar), vescovo", TYPE_OPTIONAL_MEMORY),
    (2, 5, "S. Agata, vergine e martire", TYPE_MEMORY),
    (2, 6, "Ss. Paolo Miki e compagni, martiri", TYPE_MEMORY),
    (2, 8, "S. Girolamo Emiliani", TYPE_OPTIONAL_MEMORY),
    (2, 8, "S. Giuseppina Bakhita, vergine", TYPE_OPTIONAL_MEMORY),
    (2, 10, "S. Scolastica, vergine", TYPE_MEMORY),
    (2, 11, "Beata Vergine Maria di Lourdes", TYPE_OPTIONAL_MEMORY),
    (2, 14, "Ss. Cirillo, monaco, e Metodio, vescovo, patroni d'Europa", TYPE_FEAST),
    (2, 17, "Ss. Sette Fondatori dell'Ordine dei Servi della beata Vergine Maria", TYPE_OPTIONAL_MEMORY),
    (2, 21, "S. Pier Damiani, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (2, 22, "Cattedra di S. Pietro, apostolo", TYPE_FEAST),
    (2, 23, "S. Policarpo, vescovo e martire", TYPE_MEMORY),

    (3, 4, "S. Casimiro", TYPE_OPTIONAL_MEMORY),
    (3, 7, "Ss. Perpetua e Felicita, martiri", TYPE_MEMORY),
    (3, 8, "S. Giovanni di Dio, religioso", TYPE_OPTIONAL_MEMORY),
    (3, 9, "S. Francesca Romana, religiosa", TYPE_OPTIONAL_MEMORY),
    (3, 17, "S. Patrizio, vescovo", TYPE_OPTIONAL_MEMORY),
    (3, 18, "S. Cirillo di Gerusalemme, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (3, 19, "S. Giuseppe, sposo della beata Vergine Maria", TYPE_SOLEMNITY),
    (3, 23, "S. Turibio de Mogrovejo, vescovo", TYPE_OPTIONAL_MEMORY),
    (3, 25, "Annunciazione del Signore", TYPE_SOLEMNITY),

    (4, 2, "S. Francesco de Paola, eremita", TYPE_OPTIONAL_MEMORY),
    (4, 4, "S. Isidoro, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (4, 5, "S. Vincenzo Ferrer, sacerdote", TYPE_OPTIONAL_MEMORY),
    (4, 7, "S. Giovanni Battista de la Salle, sacerdote", TYPE_MEMORY),
    (4, 11, "S. Stanislao, vescovo e martire", TYPE_MEMORY),
    (4, 13, "S. Martino I, papa e martire", TYPE_OPTIONAL_MEMORY),
    (4, 21, "S. Anselmo, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (4, 23, "S. Adalberto, vescovo e martire", TYPE_OPTIONAL_MEMORY),
    (4, 23, "S. Giorgio, martire", TYPE_OPTIONAL_MEMORY),
    (4, 24, "S. Fedele da Sigmaringen, sacerdote e martire", TYPE_OPTIONAL_MEMORY),
    (4, 25, "S. Marco, evangelista", TYPE_FEAST),
    (4, 28, "S. Luigi Maria Grignion da Montfort, sacerdote", TYPE_OPTIONAL_MEMORY),
    (4, 28, "S. Pietro Chanel, sacerdote e martire", TYPE_OPTIONAL_MEMORY),
    (4, 29, "S. Caterina da Siena, vergine e dottore della Chiesa, patrona d'Italia e d'Europa", TYPE_FEAST),
    (4, 30, "S. Pio V, papa", TYPE_OPTIONAL_MEMORY),

    (5, 1, "S. Giuseppe Lavoratore", TYPE_OPTIONAL_MEMORY),
    (5, 2, "S. Atanasio, vescovo e dottore della Chiesa", TYPE_MEMORY),
    (5, 3, "Ss. Filippo e Giacomo, apostoli", TYPE_FEAST),
    (5, 12, "Ss. Nereo e Achilleo, martiri", TYPE_OPTIONAL_MEMORY),
    (5, 12, "S. Pancrazio, martire", TYPE_OPTIONAL_MEMORY),
    (5, 13, "Beata Vergine Maria di Fatima", TYPE_OPTIONAL_MEMORY),
    (5, 14, "S. Mattia, apostolo", TYPE_FEAST),
    (5, 18, "S. Giovanni I, papa e martire", TYPE_OPTIONAL_MEMORY),
    (5, 20, "S. Bernardino da Siena, sacerdote", TYPE_OPTIONAL_MEMORY),
    (5, 21, "Ss. Cristoforo Magellanes, sacerdote, e compagni, martiri", TYPE_OPTIONAL_MEMORY),
    (5, 22, "S. Rita da Cascia, religiosa", TYPE_OPTIONAL_MEMORY),
    (5, 25, "S. Beda Venerabile, sacerdote e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (5, 25, "S. Gregorio VII, papa", TYPE_OPTIONAL_MEMORY),
    (5, 25, "S. Maria Maddalena de' Pazzi, vergine", TYPE_OPTIONAL_MEMORY),
    (5, 26, "S. Filippo Neri, sacerdoti", TYPE_MEMORY),
    (5, 27, "S. Agostino di Canterbury, vescovo", TYPE_OPTIONAL_MEMORY),
    (5, 31, "Visitazione della Beata Vergine Maria", TYPE_FEAST),

    (6, 1, "S. Giustino, martire", TYPE_MEMORY),
    (6, 2, "Ss. Marcellino e Pietro, martiri", TYPE_OPTIONAL_MEMORY),
    (6, 3, "S. Carlo Lwanga e compagni, martiri", TYPE_MEMORY),
    (6, 5, "S. Bonifacio, vescovo e martire", TYPE_MEMORY),
    (6, 6, "S. Norberto, vescovo", TYPE_OPTIONAL_MEMORY),
    (6, 9, "S. Efrem, diacono e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (6, 11, "S. Barnaba, apostolo", TYPE_MEMORY),
    (6, 13, "S. Antonio da Padova, sacerdote e dottore della Chiesa", TYPE_MEMORY),
    (6, 19, "S. Romualdo, abate", TYPE_OPTIONAL_MEMORY),
    (6, 21, "S. Luigi Gonzaga, religioso", TYPE_MEMORY),
    (6, 22, "S. Paolino da Nola, vescovo", TYPE_OPTIONAL_MEMORY),
    (6, 22, "Ss. Giovanni Fisher, vescovo, e Tommaso More, martiri", TYPE_OPTIONAL_MEMORY),
    (6, 24, "Natività di S. Giovanni Battista", TYPE_SOLEMNITY),
    (6, 27, "S. Cirillo d'Alessandria, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (6, 28, "S. Ireneo, vescovo e martire", TYPE_MEMORY),
    (6, 29, "Ss. Pietro e Paolo, apostoli", TYPE_SOLEMNITY),
    (6, 30, "Ss. Primi martiri della Chiesa di Roma", TYPE_OPTIONAL_MEMORY),

    (7, 3, "S. Tommaso, apostolo", TYPE_FEAST),
    (7, 4, "S. Elisabetta di Portogallo", TYPE_OPTIONAL_MEMORY),
    (7, 5, "S. Antonio Maria Zaccaria, sacerdote", TYPE_OPTIONAL_MEMORY),
    (7, 6, "S. Maria Goretti, vergine e martire", TYPE_OPTIONAL_MEMORY),
    (7, 9, "Ss. Agostino Zhao Rong, sacerdote, e compagni, martiri", TYPE_OPTIONAL_MEMORY),
    (7, 11, "S. Benedetto, abate, patrono d'Europa", TYPE_FEAST),
    (7, 13, "S. Enrico", TYPE_OPTIONAL_MEMORY),
    (7, 14, "S. Camillo de Lellis, sacerdote", TYPE_OPTIONAL_MEMORY),
    (7, 15, "S. Bonaventura, vescovo e dottore della Chiesa", TYPE_MEMORY),
    (7, 16, "Beata Vergine Maria del Monte Carmelo", TYPE_OPTIONAL_MEMORY),
    (7, 20, "S. Apollinare, vescovo e martire", TYPE_OPTIONAL_MEMORY),
    (7, 21, "S. Lorenzo da Brindisi, sacerdote e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (7, 22, "S. Maria Maddalena", TYPE_MEMORY),
    (7, 23, "S. Brigida, religiosa", TYPE_FEAST),
    (7, 24, "S. Charbel Makhluf, sacerdote", TYPE_OPTIONAL_MEMORY),
    (7, 25, "S. Giacomo, apostolo", TYPE_FEAST),
    (7, 26, "S. Gioacchino e Anna, genitori della Beata Vergine Maria", TYPE_MEMORY),
    (7, 29, "S. Marta", TYPE_MEMORY),
    (7, 30, "S. Pietro Crisologo, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (7, 31, "S. Ignazio di Loyola, sacerdote", TYPE_MEMORY),

    (8, 1, "S. Alfonso Maria de' Liguori, vescovo e dottore della Chiesa", TYPE_MEMORY),
    (8, 2, "S. Eusebio di Vercelli, vescovo", TYPE_OPTIONAL_MEMORY),
    (8, 2, "S. Pier Giuliano Eymard, sacerdote", TYPE_OPTIONAL_MEMORY),
    (8, 4, "S. Giovanni Maria Vianney, sacerdote", TYPE_MEMORY),
    (8, 5, "Dedicazione della Basilica di S. Maria Maggiore", TYPE_OPTIONAL_MEMORY),
    (8, 6, "Trasfigurazione del Signore", TYPE_LORD_FEAST),
    (8, 7, "Ss. Sisto II, papa, e compagni, martiri", TYPE_OPTIONAL_MEMORY),
    (8, 7, "S. Gaetano, sacerdote", TYPE_OPTIONAL_MEMORY),
    (8, 8, "S. Domenico, sacerdote", TYPE_MEMORY),
    (8, 9, "S. Teresa Benedetta della Croce, vergine e martire, patrona d'Europa", TYPE_FEAST),
    (8, 10, "S. Lorenzo, diacono e martire", TYPE_FEAST),
    (8, 11, "S. Chiara, vergine", TYPE_MEMORY),
    (8, 12, "S. Giovanna Francesca de Chantal, religiosa", TYPE_OPTIONAL_MEMORY),
    (8, 13, "Ss. Ponziano, papa, e Ippolito, sacerdote, martiri", TYPE_OPTIONAL_MEMORY),
    (8, 14, "S. Massimiliano Maria Kolbe, sacerdote e martire", TYPE_MEMORY),
    (8, 15, "Assunzione della beata Vergine Maria", TYPE_SOLEMNITY),
    (8, 16, "S. Stefano di Ungheria", TYPE_OPTIONAL_MEMORY),
    (8, 19, "S. Giovanni Eudes, sacerdote", TYPE_OPTIONAL_MEMORY),
    (8, 20, "S. Bernardo, abate e dottore della Chiesa", TYPE_MEMORY),
    (8, 21, "S. Pio X, papa", TYPE_MEMORY),
    (8, 22, "Beata Vergine Maria Regina", TYPE_MEMORY),
    (8, 23, "S. Rosa da Lima, vergine", TYPE_OPTIONAL_MEMORY),
    (8, 24, "S. Bartolomeo, apostolo", TYPE_FEAST),
    (8, 25, "S. Ludovico", TYPE_OPTIONAL_MEMORY),
    (8, 25, "S. Giuseppe Calasanzio, sacerdote", TYPE_OPTIONAL_MEMORY),
    (8, 27, "S. Monica", TYPE_MEMORY),
    (8, 28, "S. Agostino, vescovo e dottore della Chiesa", TYPE_MEMORY),
    (8, 29, "Martirio di S. Giovanni Battista", TYPE_MEMORY),

    (9, 3, "S. Gregorio Magno, papa e dottore della Chiesa", TYPE_MEMORY),
    (9, 8, "Natività della beata Vergine Maria", TYPE_FEAST),
    (9, 9, "S. Pietro Claver, sacerdote", TYPE_OPTIONAL_MEMORY),
    (9, 12, "SS. Nome di Maria", TYPE_OPTIONAL_MEMORY),
    (9, 13, "S. Giovanni Crisostomo, vescovo e dottore della Chiesa", TYPE_MEMORY),
    (9, 14, "Esaltazione della Santa Croce", TYPE_LORD_FEAST),
    (9, 15, "Beata Vergine Maria Addolorata", TYPE_MEMORY),
    (9, 16, "Ss. Cornelio, papa, e Cipriano, vescovo, martiri", TYPE_MEMORY),
    (9, 17, "S. Roberto Bellarmino, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (9, 19, "S. Gennaro, vescovo e martire", TYPE_OPTIONAL_MEMORY),
    (9, 20, "Ss. Andrea Kim Taegon, sacerdote, Paolo Chong Hasang e compagni, martiri", TYPE_MEMORY),
    (9, 21, "S. Matteo, apostolo ed evangelista", TYPE_FEAST),
    (9, 23, "S. Pio da Pietralcina, sacerdote", TYPE_MEMORY),
    (9, 26, "Ss. Cosma e Damiano, martiri", TYPE_OPTIONAL_MEMORY),
    (9, 27, "S. Vincenzo de' Paoli, sacerdote", TYPE_MEMORY),
    (9, 28, "S. Venceslao, martire", TYPE_OPTIONAL_MEMORY),
    (9, 28, "S. Lorenzo Ruiz e compagni, martiri", TYPE_OPTIONAL_MEMORY),
    (9, 29, "Ss. Michele, Gabriele e Raffaele, arcangeli", TYPE_FEAST),
    (9, 30, "S. Girolamo, sacerdote e dottore della Chiesa", TYPE_MEMORY),

    (10, 1, "S. Teresa di Gesù Bambino, vergine e dottore della Chiesa", TYPE_MEMORY),
    (10, 2, "Ss. Angeli Custodi", TYPE_MEMORY),
    (10, 4, "S. Francesco d'Assisi, patrono d'Italia", TYPE_FEAST),
    (10, 6, "S. Bruno, monaco", TYPE_OPTIONAL_MEMORY),
    (10, 7, "Beata Vergine Maria del Rosario", TYPE_MEMORY),
    (10, 9, "Ss. Dionigi, vescovo, e compagni, martiri", TYPE_OPTIONAL_MEMORY),
    (10, 9, "S. Giovanni Leonardi, sacerdote", TYPE_OPTIONAL_MEMORY),
    (10, 14, "S. Callisto I, papa e martire", TYPE_OPTIONAL_MEMORY),
    (10, 15, "S. Teresa d'Avila, vergine e dottore della Chiesa", TYPE_MEMORY),
    (10, 16, "S. Edvige, religiosa", TYPE_OPTIONAL_MEMORY),
    (10, 16, "S. Margherita Maria Alacoque, vergine", TYPE_OPTIONAL_MEMORY),
    (10, 17, "S. Ignazio di Antiochia, vescovo e martire", TYPE_MEMORY),
    (10, 18, "S. Luca, evangelista", TYPE_FEAST),
    (10, 19, "Ss. Giovanni de Brébeuf e Isacco Jogues, sacerdoti, e compagni, martiri", TYPE_OPTIONAL_MEMORY),
    (10, 19, "S. Paolo della Croce, sacerdote", TYPE_OPTIONAL_MEMORY),
    (10, 23, "S. Giovanni da Capestrano, sacerdote", TYPE_OPTIONAL_MEMORY),
    (10, 24, "S. Antonio Maria Claret, vescovo", TYPE_OPTIONAL_MEMORY),
    (10, 28, "Ss. Simone e Giuda, apostoli", TYPE_FEAST),

    (11, 1, "Tutti i santi", TYPE_SOLEMNITY),
    # Techincally "commemorazione" is not a solemnity, but it is
    # granted the same priority level
    #(11, 2, u"Commemoriazione di tutti i fedeli defunti", TYPE_SOLEMNITY),
    (11, 3, "S. Martino de Porres, religioso", TYPE_OPTIONAL_MEMORY),
    (11, 4, "S. Carlo Borromeo, vescovo", TYPE_MEMORY),
    (11, 9, "Dedicazione della Basilica Lateranense", TYPE_LORD_FEAST),
    (11, 10, "S. Leone Magno, papa e dottore della Chiesa", TYPE_MEMORY),
    (11, 11, "S. Martino di Tours, vescovo", TYPE_MEMORY),
    (11, 12, "S. Giosafat, vescovo e martire", TYPE_MEMORY),
    (11, 15, "S. Alberto Magno, vescovo e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (11, 16, "S. Margherita di Scozia", TYPE_OPTIONAL_MEMORY),
    (11, 16, "S. Geltrude, vergine", TYPE_OPTIONAL_MEMORY),
    (11, 17, "S. Elisabetta di Ungheria, religiosa", TYPE_MEMORY),
    (11, 18, "Dedicazione delle Basiliche dei Ss. Pietro e Paolo, apostoli", TYPE_OPTIONAL_MEMORY),
    (11, 21, "Presentazione della beata Vergine Maria", TYPE_MEMORY),
    (11, 22, "S. Cecilia, vergine e martire", TYPE_MEMORY),
    (11, 23, "S. Clemente I, papa e martire", TYPE_OPTIONAL_MEMORY),
    (11, 23, "S. Colombano, abate", TYPE_OPTIONAL_MEMORY),
    (11, 24, "Ss. Andrea Dung-Lac, sacerdote, e compagni, martiri", TYPE_MEMORY),
    (11, 25, "S. Caterina di Alessandria, vergine e martire", TYPE_OPTIONAL_MEMORY),
    (11, 30, "S. Andrea, apostolo", TYPE_FEAST),

    (12, 3, "S. Francesco Saverio, sacerdote", TYPE_MEMORY),
    (12, 4, "S. Giovanni Damasceno, sacerdote e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (12, 6, "S. Nicola, vescovo", TYPE_OPTIONAL_MEMORY),
    (12, 7, "S. Ambrogio, vescovo e dottore della Chiesa", TYPE_MEMORY),
    (12, 8, "Immacolata Concezione della beata Vergine Maria", TYPE_SOLEMNITY),
    (12, 9, "S. Giovanni Diego Cuauhtlatoatzin", TYPE_OPTIONAL_MEMORY),
    (12, 11, "S. Damaso I, papa", TYPE_OPTIONAL_MEMORY),
    (12, 12, "Beata Vergine Maria di Guadalupe", TYPE_OPTIONAL_MEMORY),
    (12, 13, "S. Lucia, vergine e martire", TYPE_MEMORY),
    (12, 14, "S. Giovanni della Croce, sacerdote e dottore della Chiesa", TYPE_MEMORY),
    (12, 21, "S. Pietro Canisio, sacerdote e dottore della Chiesa", TYPE_OPTIONAL_MEMORY),
    (12, 23, "S. Giovanni da Kety", TYPE_OPTIONAL_MEMORY),
    #(12, 25, u"Natale del Signore", TYPE_SOLEMNITY),
    (12, 26, "S. Stefano, primo martire", TYPE_FEAST),
    (12, 27, "S. Giovanni, apostolo ed evangelista", TYPE_FEAST),
    (12, 28, "Ss. Innocenti, martiri", TYPE_FEAST),
    (12, 29, "S. Tommaso Becket, vescovo e martire", TYPE_OPTIONAL_MEMORY),
    (12, 31, "S. Silvestro I, papa", TYPE_OPTIONAL_MEMORY),
]

# Bad, but temporary
MOVABLE_CALENDAR_STR = {
    "saint_family": [("Santa Famiglia di Gesù, Maria e Giuseppe", TYPE_LORD_FEAST)],
    "pentecost + datetime.timedelta(days=7)": [("SS. Trinità", TYPE_SOLEMNITY)],
    "pentecost + datetime.timedelta(days=14)": [("SS. Corpo e Sangue di Cristo", TYPE_SOLEMNITY)],
    "pentecost + datetime.timedelta(days=19)": [("Sacratissimo Cuore di Gesù", TYPE_SOLEMNITY)],
    "pentecost + datetime.timedelta(days=20)": [("Cuore Immacolato della beata Vergine Maria", TYPE_MEMORY)],
    }

def populate_base_competitors(session):
    import database

    # Advent
    pairs = []
    for week in [1, 2]:
        for weekday in [WD_SUNDAY, WD_MONDAY, WD_TUESDAY, WD_WEDNESDAY,
                        WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
            pairs.append((week, weekday))
    week = 3
    for weekday in [WD_SUNDAY, WD_MONDAY, WD_TUESDAY, WD_WEDNESDAY,
                    WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
        pairs.append((week, weekday))
    pairs.append((4, WD_SUNDAY))
    for week, weekday in pairs:
        title = BASE_TITLE_ITALIAN % (WEEKDAYS_ITALIAN[weekday],
                                      int_to_roman(week),
                                      SEASONS_ITALIAN[SEASON_ADVENT])
        te = database.TimedEvent()
        te.status = 'incomplete'
        te.week = week
        te.weekday = weekday
        te.season = SEASON_ADVENT
        te.title = title
        te.priority = PRI_CHRISTMAS if weekday == WD_SUNDAY else PRI_WEEKDAYS
        session.add(te)

    for day in range(17, 25):
        fe = database.FixedEvent()
        fe.status = 'incomplete'
        fe.day = day
        fe.month = 12
        fe.title = '%d dicembre' % (day)
        fe.priority = PRI_STRONG_WEEKDAYS
        session.add(fe)

    # Christmas
    fe = database.FixedEvent()
    fe.status = 'incomplete'
    fe.day = 25
    fe.month = 12
    fe.title = 'Natale del Signore'
    fe.priority = PRI_CHRISTMAS
    session.add(fe)

    # Octave after Christmas
    for day in range(29, 32):
        fe = database.FixedEvent()
        fe.status = 'incomplete'
        fe.day = day
        fe.month = 12
        fe.title = '%d dicembre' % (day)
        fe.priority = PRI_STRONG_WEEKDAYS
        session.add(fe)

    # Sunday after Christmas
    te = database.TimedEvent()
    te.status = 'incomplete'
    te.week = 2
    te.weekday = WD_SUNDAY
    te.season = SEASON_CHRISTMAS
    te.title = 'seconda domenica dopo Natale'
    te.priority = PRI_SUNDAYS
    session.add(te)

    # Weekdays in January
    for day in list(range(2, 6)) + list(range(7, 13)):
        fe = database.FixedEvent()
        fe.status = 'incomplete'
        fe.day = day
        fe.month = 1
        fe.season = SEASON_CHRISTMAS
        fe.title = '%d gennaio' % (day)
        fe.priority = PRI_WEEKDAYS
        session.add(fe)

    # Epiphany
    fe = database.FixedEvent()
    fe.status = 'incomplete'
    fe.day = 6
    fe.month = 1
    fe.title = 'Epifania del Signore'
    fe.priority = PRI_CHRISTMAS
    session.add(fe)

    # Baptism
    me = database.MovableEvent()
    me.status = 'incomplete'
    me.calc_func = 'baptism'
    me.priority = PRI_SUNDAYS
    me.title = 'Battesimo del Signore'
    session.add(me)

    # Ordinary time
    for week in range(1, 35):
        for weekday in [WD_SUNDAY, WD_MONDAY, WD_TUESDAY, WD_WEDNESDAY,
                        WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
            if (week, weekday) == (1, WD_SUNDAY):
                continue
            title = BASE_TITLE_ITALIAN % (WEEKDAYS_ITALIAN[weekday],
                                          int_to_roman(week),
                                          SEASONS_ITALIAN[SEASON_ORDINARY])
            if (week, weekday) == (34, WD_SUNDAY):
                title = "Nostro Signore Gesù Cristo Re dell'Universo"
            te = database.TimedEvent()
            te.status = 'incomplete'
            te.week = week
            te.weekday = weekday
            te.season = SEASON_ORDINARY
            te.title = title
            te.priority = PRI_SUNDAYS if weekday == WD_SUNDAY else PRI_WEEKDAYS
            session.add(te)

    # Ash day and following ones
    for weekday in [WD_WEDNESDAY, WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
        te = database.TimedEvent()
        te.status = 'incomplete'
        te.season = SEASON_LENT
        te.week = 0
        te.weekday = weekday
        te.priority = PRI_CHRISTMAS if weekday == WD_WEDNESDAY else PRI_STRONG_WEEKDAYS
        te.title = 'Mercoledì delle Ceneri' if weekday == WD_WEDNESDAY else '%s dopo le Ceneri' % (WEEKDAYS_ITALIAN[weekday])
        session.add(te)

    # Lent
    for week in range(1, 6):
        for weekday in [WD_SUNDAY, WD_MONDAY, WD_TUESDAY, WD_WEDNESDAY,
                        WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
            title = BASE_TITLE_ITALIAN % (WEEKDAYS_ITALIAN[weekday],
                                          int_to_roman(week),
                                          SEASONS_ITALIAN[SEASON_LENT])
            te = database.TimedEvent()
            te.status = 'incomplete'
            te.week = week
            te.weekday = weekday
            te.season = SEASON_LENT
            te.title = title
            te.priority = PRI_CHRISTMAS if weekday == WD_SUNDAY else PRI_STRONG_WEEKDAYS
            session.add(te)

    # Holy week
    for weekday in [WD_SUNDAY, WD_MONDAY, WD_TUESDAY, WD_WEDNESDAY,
                    WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
        title = 'Domenica delle Palme' if weekday == WD_SUNDAY else '%s Santo' % (WEEKDAYS_ITALIAN[weekday].capitalize())
        te = database.TimedEvent()
        te.status = 'incomplete'
        te.week = 6
        te.weekday = weekday
        te.season = SEASON_LENT
        te.title = title
        te.priority = PRI_TRIDUUM if weekday in [WD_THURSDAY, WD_FRIDAY, WD_SATURDAY] else PRI_CHRISTMAS
        session.add(te)

    # Easter
    for weekday in [WD_SUNDAY, WD_MONDAY, WD_TUESDAY, WD_WEDNESDAY,
                    WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
        title = 'Pasqua di Resurrezione' if weekday == WD_SUNDAY else "%s fra l'ottava di Pasqua" % (WEEKDAYS_ITALIAN[weekday])
        te = database.TimedEvent()
        te.status = 'incomplete'
        te.week = 1
        te.weekday = weekday
        te.season = SEASON_EASTER
        te.title = title
        te.priority = PRI_TRIDUUM if weekday == WD_SUNDAY else PRI_CHRISTMAS
        session.add(te)

    # Easter time
    for week in range(2, 8):
        for weekday in [WD_SUNDAY, WD_MONDAY, WD_TUESDAY, WD_WEDNESDAY,
                        WD_THURSDAY, WD_FRIDAY, WD_SATURDAY]:
            title = BASE_TITLE_ITALIAN % (WEEKDAYS_ITALIAN[weekday],
                                          int_to_roman(week),
                                          SEASONS_ITALIAN[SEASON_EASTER])
            if (week, weekday) == (7, WD_SUNDAY):
                title = 'Ascensione del Signore'
            te = database.TimedEvent()
            te.status = 'incomplete'
            te.week = week
            te.weekday = weekday
            te.season = SEASON_EASTER
            te.title = title
            te.priority = PRI_CHRISTMAS if weekday == WD_SUNDAY else PRI_WEEKDAYS
            session.add(te)

    # Pentecost
    te = database.TimedEvent()
    te.status = 'incomplete'
    te.week = 8
    te.weekday = WD_SUNDAY
    te.season = SEASON_EASTER
    te.title = 'Domenica di Pentecoste'
    te.priority = PRI_CHRISTMAS
    session.add(te)

def populate_database():
    import database
    database.Base.metadata.create_all()
    session = database.Session()

    for month, day, title, type_ in GENERAL_CALENDAR_LIST:
        fe = database.FixedEvent()
        fe.status = 'incomplete'
        fe.day = day
        fe.month = month
        fe.type = type_
        fe.title = title
        session.add(fe)

    for calc_func, data in MOVABLE_CALENDAR_STR.items():
        me = database.MovableEvent()
        me.status = 'incomplete'
        me.type = data[0][1]
        me.title = data[0][0]
        me.calc_func = calc_func
        session.add(me)

    populate_base_competitors(session)

    # An exception
    fe = database.FixedEvent()
    fe.status = 'incomplete'
    fe.day = 2
    fe.month = 11
    fe.priority = PRI_SOLEMNITIES
    fe.title = 'Commemorazione di tutti i fedeli defunti'
    session.add(fe)

    session.commit()

if __name__ == '__main__':
    populate_database()
