#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re

from database import Session, Reading
from quote import BibleQuery, decode_quote, convert_quote_psalm_numbering
from utils import PrependStream
from editor import Editor

def main():
    reading_id = int(sys.argv[1])
    session = Session()
    bible_query = BibleQuery()
    reading = session.query(Reading).filter(Reading.id == reading_id).one()
    text = reading.text if reading.text is not None else ""

    editor = Editor()

    # Fix wrong quotation marks
    text = re.sub(r'"([a-zA-ZàòùèéìÒÀÙÈÉÌ0-9])', r'“\1', text, count=0)
    text = re.sub(r'([a-zA-ZàòùèéìÒÀÙÈÉÌ0-9\.?!])"', r'\1”', text, count=0)

    # From http://stackoverflow.com/questions/15120346/emacs-setting-comment-character-by-file-extension
    PrependStream(editor.tempfile, '# ').write('-*- coding: utf-8; comment-start: "#"; -*-\n')
    PrependStream(editor.tempfile, '# ').write('Quote: %s\n' % (reading.quote))
    editor.tempfile.write('\n')
    editor.tempfile.write(text)
    editor.tempfile.write('\n')
    PrependStream(editor.tempfile, '# ').write('Useful characters: “”–\n\n')
    try:
        converted_quote = convert_quote_psalm_numbering(reading.quote, False)
        bible_text = bible_query.get_text(decode_quote(converted_quote, allow_only_chap=True))
    except:
        PrependStream(editor.tempfile, '# ').write('Quote: %s\nCould not retrieve bible text\n' % (reading.quote))
        print(decode_quote(reading.quote, allow_only_chap=True))
        raise
    else:
        bible_text = "\n".join([x.strip() for x in bible_text.split('\n')])
        PrependStream(editor.tempfile, '# ').write('Quote: %s\nConverted quote: %s\nBible text:\n\n%s' % (reading.quote, converted_quote, bible_text))

    editor.edit()

    new_text = ''.join([x for x in editor.edited_content if not x.startswith('#')]).strip() + '\n'

    if editor.confirmation_request(new_text != reading.text):
        reading.text = new_text
        session.commit()
    else:
        session.rollback()

if __name__ == '__main__':
    main()
