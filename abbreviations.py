#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It is hoped that Psalms appear everywhere in the same abbreviated
# from
PSALMS_ABBR = "Sal"

# IMPORTANT: We're using Vatican abbreviations for the databases

BOOKS = [
    "Genesi",
    "Esodo",
    "Levitico",
    "Numeri",
    "Deuteronomio",

    "Giosuè",
    "Giudici",
    "Rut",
    "1 Samuele",
    "2 Samuele",
    "1 Re",
    "2 Re",

    "1 Cronache",
    "2 Cronache",
    "Esdra",
    "Neemia",

    "Tobia",
    "Giuditta",
    "Ester",

    "1 Maccabei",
    "2 Maccabei",

    "Giobbe",
    "Salmi",
    "Proverbi",
    "Qoélet",

    "Cantico dei Cantici",

    "Sapienza",

    "Siracide",

    "Isaia",
    "Geremia",
    "Lamentazioni",
    "Baruc",
    "Ezechiele",
    "Daniele",
    "Osea",
    "Gioele",
    "Amos",
    "Abdia",
    "Giona",
    "Michea",
    "Naum",
    "Abacuc",
    "Sofonia",
    "Aggeo",
    "Zaccaria",
    "Malachia",

    "Matteo",
    "Marco",
    "Luca",

    "Giovanni",

    "Atti degli Apostoli",

    "Romani",
    "1 Corinzi",
    "2 Corinzi",
    "Galati",
    "Efesini",
    "Filippesi",
    "Colossesi",
    "1 Tessalonicesi",
    "2 Tessalonicesi",
    "1 Timoteo",
    "2 Timoteo",
    "Tito",
    "Filemone",
    "Ebrei",

    "Giacomo",
    "1 Pietro",
    "2 Pietro",
    "1 Giovanni",
    "2 Giovanni",
    "3 Giovanni",
    "Giuda",

    "Apocalisse"
    ]

ABBR_GERUSALEMME = [
    "Gen",
    "Es",
    "Lv",
    "Nm",
    "Dt",

    "Gs",
    "Gdc",
    "Rt",
    "1Sam",
    "2Sam",
    "1Re",
    "2Re",

    "1Cr",
    "2Cr",
    "Esd",
    "Ne",

    "Tb",
    "Gdt",
    "Est",

    "1Mac",
    "2Mac",

    "Gb",
    "Sal",
    "Pr",
    "Qo",

    "Ct",

    "Sap",

    "Sir",

    "Is",
    "Ger",
    "Lam",
    "Bar",
    "Ez",
    "Dn",
    "Os",
    "Gl",
    "Am",
    "Abd",
    "Gn",
    "Mi",
    "Na",
    "Ab",
    "Sof",
    "Ag",
    "Zc",
    "Ml",

    "Mt",
    "Mc",
    "Lc",

    "Gv",

    "At",

    "Rm",
    "1Cor",
    "2Cor",
    "Gal",
    "Ef",
    "Fil",
    "Col",
    "1Ts",
    "2Ts",
    "1Tm",
    "2Tm",
    "Tt",
    "Fm",
    "Eb",

    "Gc",
    "1Pt",
    "2Pt",
    "1Gv",
    "2Gv",
    "3Gv",
    "Gd",

    "Ap"
    ]

ABBR_VATICAN = [
    "Gn",
    "Es",
    "Lv",
    "Nm",
    "Dt",

    "Gs",
    "Gdc",
    "Rt",
    "1Sam",
    "2Sam",
    "1Re",
    "2Re",

    "1Cr",
    "2Cr",
    "Esd",
    "Ne",

    "Tb",
    "Gdt",
    "Est",

    "1Mac",
    "2Mac",

    "Gb",
    "Sal",
    "Prv",
    "Qo",

    "Ct",

    "Sap",

    "Sir",

    "Is",
    "Ger",
    "Lam",
    "Bar",
    "Ez",
    "Dn",
    "Os",
    "Gl",
    "Am",
    "Abd",
    "Gio",
    "Mic",
    "Na",
    "Ab",
    "Sof",
    "Ag",
    "Zc",
    "Ml",

    "Mt",
    "Mc",
    "Lc",

    "Gv",

    "At",

    "Rm",
    "1Cor",
    "2Cor",
    "Gal",
    "Ef",
    "Fil",
    "Col",
    "1Ts",
    "2Ts",
    "1Tm",
    "2Tm",
    "Tt",
    "Fm",
    "Eb",

    "Gc",
    "1Pt",
    "2Pt",
    "1Gv",
    "2Gv",
    "3Gv",
    "Gd",

    "Ap"
    ]
