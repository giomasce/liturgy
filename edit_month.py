#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import datetime
import json
import traceback

from sqlalchemy.orm.session import object_session

from liturgy import get_lit_date, print_lit_date
from database import Session, Reading, from_dict, session_has_pending_commit
from quote import BibleQuery, decode_quote, convert_quote_psalm_numbering
from utils import PrependStream, real_itermonthdays
from editor import Editor

def edit_month(year, month, single_day=None):
    session = Session()
    bible_query = BibleQuery()
    lit_years = {}

    editor = Editor()

    # From http://stackoverflow.com/questions/15120346/emacs-setting-comment-character-by-file-extension
    PrependStream(editor.tempfile, '# ').write('-*- coding: utf-8; comment-start: "#"; -*-\n')
    editor.tempfile.write('\n')

    def push_day(day):
        date = datetime.date(year, month, day)
        lit_date = get_lit_date(date, lit_years, session)
        events = [x[1] for x in lit_date.competitors]
        print_lit_date(lit_date, PrependStream(editor.tempfile, '# '), with_id=True)
        editor.tempfile.write('\n')
        editor.tempfile.write(json.dumps([x.as_dict() for x in events], ensure_ascii=False, indent=2, sort_keys=True) + '\n')
        editor.tempfile.write('---===---\n')
        editor.tempfile.write('\n')

    if single_day is not None:
        push_day(single_day)
    else:
        for day in real_itermonthdays(year, month):
            push_day(day)

    editor.edit()

    while True:
        lines = [x for x in editor.edited_content if not x.startswith('#')]
        buf = ''

        try:
            for line in lines:
                if line.strip() == '---===---':
                    data = json.loads(buf)
                    for piece in data:
                        from_dict(piece, session)
                    buf = ''
                else:
                    buf += line
            session.flush()

        except:
            traceback.print_exc()
            sys.stdout.write("Error while parsing new content. Re-edit? [Y/n] ")
            answer = sys.stdin.readline().strip()
            if answer != '':
                answer = answer[0]
            if answer == 'n' or answer == 'N':
                sys.stdout.write("Aborting\n")
                sys.exit(0)
            else:
                sys.stdout.write("Re-editing...\n")
                session.rollback()
                edited_content = editor.edited_content
                editor = Editor()
                editor.tempfile.write("".join(edited_content))
                editor.edit()

        else:
            break

    if editor.confirmation_request(session_has_pending_commit(session)):
        #reading.text = new_text
        session.commit()
    else:
        session.rollback()

if __name__ == '__main__':
    edit_month(*list(map(int, sys.argv[1:])))
